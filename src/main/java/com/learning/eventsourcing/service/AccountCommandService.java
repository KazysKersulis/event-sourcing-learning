package com.learning.eventsourcing.service;

import com.learning.eventsourcing.dto.AccountCreateDTO;
import com.learning.eventsourcing.dto.MoneyCreditDTO;
import com.learning.eventsourcing.dto.MoneyDebitDTO;

import java.util.concurrent.CompletableFuture;

public interface AccountCommandService {

    public CompletableFuture<String> createAccount(AccountCreateDTO accountCreateDTO);
    public CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO moneyCreditDTO);
    public CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO moneyDebitDTO);
}
